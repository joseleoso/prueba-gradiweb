# Gradiweb Backend Test - PHP

## Instrucciones

* Ingresa al directorio donde se va a instalar el software
* Ejecuta `git clone https://gitlab.com/joseleoso/prueba-gradiweb.git`.
* Ingresa a la carpeta del repositorio `cd prueba-gradiweb`.
* El proyecto está en Laravel 5.7, por lo que necesita PHP 7.2 en adelante. 

## Configuración

* Ejecuta `php composer.phar install` o si composer está instalado globalmente `composer install`.
* Ejecuta `cp .env.example .env`.
* Ejecuta `php artisan key:generate`.
* Para que funcione correctamente, ejecuta `php -S localhost:8000 -t public`.

## Rutas
* Crear Servicios = `http://localhost:8000/api/servicios`.
Debe crearse en POST, preferiblemente en x-www-form-urlencoded (si se prueba en Postman), se deben enviar los parámetros 
> 'nombre' (string), 
> 'url_imagen' (url), 
> 'dias_trabajo' (se deben colocar las palabras 'Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado' [sin comillas] y separadas por comas).

* Ver Servicios = `http://localhost:8000/api/servicios/{idServicio}`.
Debe generarse en PUT, el parámetro 'idServicio' es el id del servicio, el cual será generado cuando se crea el servicio, o cuando se traen los servicios (es la clave de cada arreglo).

* Actualizar Servicios = `http://localhost:8000/api/servicios/{idServicio}`.
Debe generarse en PUT, preferiblemente en x-www-form-urlencoded (si se prueba en Postman), el parámetro 'idServicio' es el id del servicio, el cual será generado cuando se crea el servicio, o cuando se traen los servicios (es la clave de cada arreglo). Se deben enviar los parámetros 
> 'nombre' (string), 
> 'url_imagen' (url), 
> 'dias_trabajo' (se deben colocar las palabras 'Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado' [sin comillas] y separadas por comas).

* Borrar Servicios = `http://localhost:8000/api/servicios/{idServicio}`.
Debe generarse en DELETE, preferiblemente en x-www-form-urlencoded (si se prueba en Postman), el parámetro 'idServicio' es el id del servicio, el cual será generado cuando se crea el servicio, o cuando se traen los servicios (es la clave de cada arreglo).

* Ver Servicios = `http://localhost:8000/api/servicios`.
Debe generarse en GET, traerá todos los servicios creados. 

* Ver Servicios por día = `http://localhost:8000/api/servicios/{diasemana}`.
Debe generarse en POST, el parámetro 'diasemana' debe ser un día de la semana donde 1 es lunes y 7 es domingo.

* Crear Reservas =  `http://localhost:8000/api/reservas/{idServicio}`.
Debe generarse en POST, el parámetro 'idServicio' es el id del servicio, el cual será generado cuando se crea el servicio, o cuando se traen los servicios (es la clave de cada arreglo). Se debe enviar los siguientes parámetros:
> 'fecha' Fecha de la reserva, debe ser en formato YYYY-MM-DD y no debe ser anterior al día actual.
> 'nombre' Nombre de la Persona.
> 'documento' Documento de la Persona.

* Obtener reservas por fecha =  `http://localhost:8000/api/reservas/{idServicio}`.
Debe generarse en POST. Se debe enviar los siguientes parámetros:
> 'fecha_desde' Fecha inicial, debe ser en formato YYYY-MM-DD.
> 'fecha_hasta' Fecha final, debe ser en formato YYYY-MM-DD.

***Muchas Gracias!***
